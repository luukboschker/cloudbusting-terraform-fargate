{
    "$schema": "https://app.cloudbusting.it/assets/schemas/blueprint-v1.0.schema.json",
    "resources": [
        {
            "resourceType": "alb",
            "title": "Application Load Balancer",
            "icon": "/images/Elastic-Load-Balancing.svg",
            "rules": {
                "allowedConnections": [
                    { "to": "ecs-service", "defaultName": "path" }
                ]
            }
        },
        {
            "resourceType": "ecs-fargate-cluster",
            "title": "Fargate Cluster",
            "icon": "/images/AWS-Fargate_light-bg.svg",
            "dimension": {
                "width": 200,
                "height": 150
            },
            "rules": {
                "allowedChildren": [
                    "ecs-service"
                ]
            }
        },
        {
            "resourceType": "ecs-service",
            "title": "ECS Service",
            "icon": "/images/Amazon-Elastic-Container-Service_Service_light-bg.svg",
            "rules": {
                "allowedParents": [
                    "ecs-fargate-cluster"
                ],
                "allowedConnections": [
                    { "to": "sns" },
                    { "to": "sqs" },
                    { "to": "dynamodb" },
                    { "to": "rds" }
                ],
                "settings": [
                    {
                        "name": "cpu",
                        "type": "select",
                        "label": "CPU",
                        "required": true,
                        "options": "0.25 vCPU=256, 0.5 vCPU=512, 1 vCPU=1024, 2 vCPU=2048, 4 vCPU=4096"
                    },
                    {
                        "name": "memory",
                        "type": "select",
                        "label": "Memory",
                        "required": true,
                        "options": "0.5GB=512, 1GB=1024, 2GB=2048, 3GB=3072, 4GB=4096, 5GB=5120, 6GB=6144"
                    },
                    {
                        "name": "minimum",
                        "type": "text",
                        "label": "Minimum #",
                        "required": true
                    },
                    {
                        "name": "maximum",
                        "type": "text",
                        "label": "Maximum #",
                        "required": true
                    }
                ]
            }
        },
        {
            "resourceType": "sns",
            "title": "Simple Notification Service (topic)",
            "icon": "/images/Amazon-Simple-Notification-Service-SNS.svg",
            "rules": {
                "allowedConnections": [
                    { 
                        "to": "sqs",
                        "defaultName": "all",
                        "settings": [
                            {
                                "type": "text",
                                "name": "filterPolicy",
                                "label": "Filter policy",
                                "hint": "{json}"
                            }
                        ]
                    }
                ]
            }
        },
        {
            "resourceType": "sqs",
            "title": "Simple Queue Service (queue)",
            "icon": "/images/Amazon-Simple-Queue-Service-SQS.svg"
        },
        {
            "resourceType": "dynamodb",
            "title": "DynamoDB table",
            "icon": "/images/Amazon-DynamoDB.svg",
            "rules": {
                "settings": [
                    {
                        "name": "partitionKey",
                        "type": "text",
                        "label": "Partition key",
                        "required": true
                    },
                    {
                        "name": "partitionKeyType",
                        "type": "select",
                        "label": "Type",
                        "required": true,
                        "options": "String=S, Number=N, Binary=B"
                    },
                    {
                        "name": "sortKey",
                        "type": "text",
                        "label": "Sort key"
                    },
                    {
                        "name": "sortKeyType",
                        "type": "select",
                        "label": "Type",
                        "options": "String=S, Number=N, Binary=B"
                    },
                    {
                        "name": "ttl",
                        "type": "text",
                        "label": "TTL attribute"
                    },
                    {
                        "name": "gsi",
                        "type": "list",
                        "label": "Global secondary indexes",
                        "itemTitle": "indexName",
                        "list": [
                            {
                                "name": "indexName",
                                "type": "text",
                                "label": "Index name",
                                "required": true
                            },
                            {
                                "name": "partitionKey",
                                "type": "text",
                                "label": "Partition key",
                                "required": true
                            },
                            {
                                "name": "partitionKeyType",
                                "type": "select",
                                "label": "Type",
                                "required": true,
                                "options": "String=S, Number=N, Binary=B"
                            },
                            {
                                "name": "sortKey",
                                "type": "text",
                                "label": "Sort key"
                            },
                            {
                                "name": "sortKeyType",
                                "type": "select",
                                "label": "Type",
                                "options": "String=S, Number=N, Binary=B"
                            }
                        ]
                    },
                    {
                        "name": "lsi",
                        "type": "list",
                        "label": "Local secondary indexes",
                        "itemTitle": "indexName",
                        "list": [
                            {
                                "name": "indexName",
                                "type": "text",
                                "label": "Index name",
                                "required": true
                            },
                            {
                                "name": "partitionKey",
                                "type": "text",
                                "label": "Partition key",
                                "required": true
                            },
                            {
                                "name": "partitionKeyType",
                                "type": "select",
                                "label": "Type",
                                "required": true,
                                "options": "String=S, Number=N, Binary=B"
                            },
                            {
                                "name": "sortKey",
                                "type": "text",
                                "label": "Sort key"
                            },
                            {
                                "name": "sortKeyType",
                                "type": "select",
                                "label": "Type",
                                "options": "String=S, Number=N, Binary=B"
                            }
                        ]
                    }
                ]
            }
        },
        {
            "resourceType": "rds",
            "title": "Aurora PostgresQL database",
            "icon": "/images/Amazon-RDS_light-bg.svg",
            "rules": {
                "settings": [
                    {
                        "name": "databasename",
                        "type": "text",
                        "label": "Database name",
                        "required": true
                    },
                    {
                        "name": "instancetype",
                        "type": "select",
                        "label": "Instance type",
                        "required": true,
                        "options": "2vCPU - 4GB=db.t3.medium, 2vCPU - 16GB/=db.r5.large, 4vCPU - 32GB=db.r5.xlarge"
                    }
                ]
            }
        }
    ]
}
