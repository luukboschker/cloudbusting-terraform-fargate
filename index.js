#!/usr/bin/env node
const cli = require('cli');
const fs = require('fs');
const modelbuilder = require('@cloudbusting/modelbuilder');
const handlebars = require('@cloudbusting/handlebars');

const helpers = require('./helpers');

cli.enable('status');

const templateDir = `${__dirname}/templates`;

const options = cli.parse({
  infraplan: ['i', 'The infraplan file to process', 'file'],
  output: ['o', 'Generated terraform files will be put here', 'string', './terraform']
});

// Initialize our own handlebar helpers
require('./handlebar-helpers');

// Read the infraplan file
cli.info(`Reading infraplan from ${options.infraplan}...`);
const infraplan = JSON.parse(fs.readFileSync(options.infraplan));

// TODO: check if name and version match

const model = modelbuilder.build(infraplan);
helpers.terraformalize(model);

cli.info(`Generate terraform files in ${options.output}...`);
cli.debug(`Template directory: ${templateDir}`);

handlebars.generate(model, {
  templateDir,
  outputDir: options.output,
  extension: '.tf'
});

cli.info('Done!');