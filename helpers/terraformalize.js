/**
 * Normalize the model so it can be used in Terraform templates.
 * In effect, this will make sure the names will only contain alphanumeric characters or hypens.
 * 
 * @param {Object} model 
 */
function terraformalize(model) {
  model.forEach(item => {
    item.name = normalize(item.name);
    if (item.parent) {
      item.parent.name = normalize(item.parent.name);
    }

    item.incoming.forEach(item => {
      item.name = normalize(item.name);
      item.from.name = normalize(item.from.name);
    });

    item.outgoing.forEach(item => {
      item.name = normalize(item.name);
      item.to.name = normalize(item.to.name);
    })
  });
}

function normalize(value) {
  return value 
    ? value.toString().toLowerCase()
      .replace(/\s+/g, '-')           // Replace spaces with -
      .replace(/_/g, '-')             // Replace hyphen with -
      .replace(/\./g, '-')            // Replace dots with -
      .replace(/\//g, '-')            // Replace slash with -
      .replace(/\\/g, '-')            // Replace backslash with -
      .replace(/[^\w\-]+/g, '')       // Remove all non-word chars
      .replace(/\-\-+/g, '-')         // Replace multiple - with single -
      .replace(/^-+/, '')             // Trim - from start of text
      .replace(/-+$/, '')             // Trim - from end of text
    : null;
}

exports.terraformalize = terraformalize;