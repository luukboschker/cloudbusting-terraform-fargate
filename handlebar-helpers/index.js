const Handlebars = require('@cloudbusting/handlebars').Handlebars;

const dyhamodb = require('./dynamodb');

Handlebars.registerHelper('dynamoDbAttributes', dyhamodb.getAttributes);