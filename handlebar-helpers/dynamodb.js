/**
 * Handlebar helper that creates an array with all key attributes in a dynamodb resource.
 * This is because the key attributes for both the table itself and it's indexes have to be defined in a DynamoDb resource.
 * 
 * @param {any} data 
 * @param {any} options 
 */
function getAttributes(data, options) {
  const result = [];

  addKeyAttributes(result, data);

  if (data.gsi) {
    data.gsi.forEach(index => addKeyAttributes(result, index));
  }

  if (data.lsi) {
    data.lsi.forEach(index => addKeyAttributes(result, index));
  }

  return result;
}

function addKeyAttributes(result, object) {
  add(result, createAttribute(object.partitionKey, object.partitionKeyType));

  if (object.sortKey) {
    add(result, createAttribute(object.sortKey, object.sortKeyType));
  }
}

function add(result, attribute) {
  if (result.some(field => field.name === attribute.name)) {
    return;
  }
  
  result.push(attribute);
}

function createAttribute (name, type) {
  return ({ name, type });
} 

exports.getAttributes = getAttributes;