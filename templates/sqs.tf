resource "aws_sqs_queue" "{{name}}" {
  name                      = "{{name}}-queue"

  redrive_policy = jsonencode({
    deadLetterTargetArn = aws_sqs_queue.{{name}}-deadletter-queue.arn
    maxReceiveCount     = 4
  })

  tags = {
    {{> default-tags.tf }}
  }
}

resource "aws_sqs_queue" "{{name}}-deadletter-queue" {
  name                      = "{{name}}-deadletter-queue"
  message_retention_seconds = 1209600

  tags = {
    {{> default-tags.tf }}
  }
}
