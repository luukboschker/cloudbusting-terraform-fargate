resource "aws_ecr_repository" "{{name}}" {
  name = "{{name}}"
  tags = {
    {{> default-tags.tf }}
  }
}

# The task definition for this service
resource "aws_ecs_task_definition" "{{name}}" {
  family                   = "{{name}}-task"
  execution_role_arn       = aws_iam_role.ecs_task_execution_role.arn
  network_mode             = "awsvpc"
  requires_compatibilities = ["FARGATE"]
  cpu                      = {{cpu}}
  memory                   = {{memory}}
  container_definitions    = <<EOF
    [
      {
        "name": "{{name}}",
        "image": "${aws_ecr_repository.{{name}}.repository_url}",  {{! Use the ECR repository created before }}
        "networkMode": "awsvpc",
        "cpu": {{cpu}},
        "memory": {{memory}},
        "portMappings": [
          {
            "containerPort": 80,
            "hostPort": 80
          }
        ],
        "logConfiguration": {
            "logDriver": "awslogs",
            "options": {
              "awslogs-group": "/ecs/{{name}}",
              "awslogs-region": "${var.aws_region}",
              "awslogs-stream-prefix": "ecs"
            }
        }
      }
    ]
    EOF
  depends_on = [
    aws_cloudwatch_log_group.{{name}}
  ]
  tags = {
    {{> default-tags.tf }}
  }
}

# Container output is logged to CloudWatch
resource "aws_cloudwatch_log_group" "{{name}}" {
  name              = "/ecs/{{name}}"
  retention_in_days = 7

  tags = {
    {{> default-tags.tf }}
    Name = "{{name}}-log-group"
  }
}

resource "aws_cloudwatch_log_stream" "{{name}}" {
  name           = "{{name}}"
  log_group_name = aws_cloudwatch_log_group.{{name}}.name
}

# The ECS service definition that runs the task 
resource "aws_ecs_service" "{{name}}" {
  name            = "{{name}}"
  cluster         = aws_ecs_cluster.{{parent.name}}.id
  task_definition = aws_ecs_task_definition.{{name}}.arn
  desired_count   = {{minimum}}
  launch_type     = "FARGATE"
  tags = {
    {{> default-tags.tf }}
  }

  lifecycle {
    ignore_changes = [
      # This can vary due to the autoscaling policy
      desired_count
    ]
  }

  network_configuration {
    security_groups   = [aws_security_group.{{name}}.id]
    subnets           = data.aws_subnet_ids.default.ids
    assign_public_ip  = true
  }

  {{! Attach to all incoming alb target groups }}
  {{#each incoming}}
  {{#is from.resourceType 'alb'}}
    load_balancer {
      target_group_arn = aws_alb_target_group.{{from.name}}-{{@root.name}}.id
      container_name = "{{@root.name}}"
      container_port = 80
    }
  {{/is}}
  {{/each}}

  depends_on = [
    {{#each incoming}}
    {{#is from.resourceType 'alb'}}
        aws_alb_listener.{{from.name}},
    {{/is}}
    {{/each}}
    aws_iam_role_policy_attachment.ecs_task_execution_role
  ]
}

# Security group that defines access rules for this service
resource "aws_security_group" "{{name}}" {
  name = "service-{{name}}"
  description = "Allow inbound access to service {{name}}"
  vpc_id = data.aws_vpc.default.id
  tags = {
    {{> default-tags.tf }}
  }

  ingress {
    protocol = "tcp"
    from_port = 80
    to_port = 80
    # Incoming only from load balancers linked to this service
    security_groups = [
      {{#each incoming}}
      {{#is from.resourceType 'alb'}}
        aws_security_group.{{from.name}}.id
      {{/is}}
      {{/each}}
    ]
  }

  egress {
    # Outgoing access to all
    protocol = "-1"
    from_port = 0
    to_port = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
}

{{! Create target groups for all incoming ALBs }}
{{#each incoming}}
{{#is from.resourceType 'alb'}}
  # Target group for the {{from.name}} load balancer
  resource "aws_alb_target_group" "{{from.name}}-{{@root.name}}" {
    name        = "{{from.name}}-target-group"
    port        = 80
    protocol    = "HTTP"
    vpc_id      = data.aws_vpc.default.id
    target_type = "ip"
    tags = {
      {{> default-tags.tf }}
    }

    health_check {
      healthy_threshold   = 3
      interval            = 30
      protocol            = "HTTP"
      matcher             = 200
      timeout             = 3
      path                = "/"
      unhealthy_threshold = 2
    }
  }

  # Listener rules for {{from.name}} load balancer
  resource "aws_lb_listener_rule" "{{from.name}}-{{@root.name}}" {
    listener_arn        = aws_alb_listener.{{from.name}}.arn
    
    action {
      type              = "forward"
      target_group_arn  = aws_alb_target_group.{{from.name}}-{{@root.name}}.arn
    }

    condition {
      path_pattern {
        values          = [
          {{#if name}}
            "/{{name}}", 
            "/{{name}}/*"
          {{else}}
            "*"
          {{/if}}
        ]
      }
    }  
  }
{{/is}}
{{/each}}

# Autoscaling rules
resource "aws_appautoscaling_target" "{{name}}" {
  service_namespace = "ecs"
  resource_id = "service/{{parent.name}}/{{name}}"
  scalable_dimension = "ecs:service:DesiredCount"
  min_capacity = {{minimum}}
  max_capacity = {{maximum}}

  depends_on = [
    aws_ecs_service.{{name}}
  ]
}

# Scale up on high CPU alarm
resource "aws_cloudwatch_metric_alarm" "cpu_high" {
  alarm_name = "{{name}}-high-cpu"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods = "2"
  metric_name = "CPUUtilization"
  namespace = "AWS/ECS"
  period = "60"
  statistic = "Average"
  threshold  ="85"

  dimensions = {
    ClusterName = "{{parent.name}}"
    ServiceName = "{{name}}"
  }

  alarm_actions = [aws_appautoscaling_policy.scale_up.arn]
}

resource "aws_appautoscaling_policy" "scale_up" {
  name = "{{name}}-scale-up"
  service_namespace = "ecs"
  resource_id = "service/{{parent.name}}/{{name}}"
  scalable_dimension = "ecs:service:DesiredCount"

  step_scaling_policy_configuration {
    adjustment_type = "ChangeInCapacity"
    cooldown = 60
    metric_aggregation_type = "Maximum"

    step_adjustment {
      metric_interval_lower_bound = 0
      scaling_adjustment = 1
    }
  }

  depends_on = [aws_appautoscaling_target.{{name}}]
}

# Scale down on low CPU
resource "aws_cloudwatch_metric_alarm" "cpu_low" {
  alarm_name = "{{name}}-low-cpu"
  comparison_operator = "LessThanOrEqualToThreshold"
  evaluation_periods = "2"
  metric_name = "CPUUtilization"
  namespace = "AWS/ECS"
  period = "60"
  statistic = "Average"
  threshold = "10"

  dimensions = {
    ClusterName = "{{parent.name}}"
    ServiceName = "{{name}}"
  }

  alarm_actions = [aws_appautoscaling_policy.scale_down.arn]
}

resource "aws_appautoscaling_policy" "scale_down" {
  name = "{{name}}-scale-down"
  service_namespace = "ecs"
  resource_id = "service/{{parent.name}}/{{name}}"
  scalable_dimension = "ecs:service:DesiredCount"

  step_scaling_policy_configuration {
    adjustment_type = "ChangeInCapacity"
    cooldown = 60
    metric_aggregation_type = "Maximum"

    step_adjustment {
      metric_interval_lower_bound = 0
      scaling_adjustment = -1
    }
  }

  depends_on = [aws_appautoscaling_target.{{name}}]
}