# Load-balancer in the public subnets
resource "aws_alb" "{{name}}" {
  name            = "{{name}}"
  subnets         = data.aws_subnet_ids.default.ids
  security_groups = [aws_security_group.{{name}}.id]
  tags = {
    {{> default-tags.tf }}
  }
}

# The listener for this load balancer
resource "aws_alb_listener" "{{name}}" {
  load_balancer_arn = aws_alb.{{name}}.id
  port              = 80
  protocol          = "HTTP"

  # By default, this load balancer will return a 404. 
  # Additional actions are added in the ecs-service resources.
  default_action {
    type            = "fixed-response"

    fixed_response {
      content_type  = "text/plain"
      message_body  = "Not found"
      status_code   = "404"
    }
  }
}

# Load balancer protection
resource "aws_security_group" "{{name}}" {
  name        = "alb-{{name}}"
  description = "Allow inbound access to ALB {{name}}"
  vpc_id      = data.aws_vpc.default.id
  tags = {
    {{> default-tags.tf }}
  }

  ingress {
    # Incoming access for http traffic
    protocol = "tcp"
    from_port = 80
    to_port = 80
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    # Outgoing access to all
    protocol = "-1"
    from_port = 0
    to_port = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
}
