resource "aws_sns_topic" "{{name}}" {
  name = "{{name}}-topic"
  tags = {
    {{> default-tags.tf }}
  }
}

{{#each outgoing }}
{{#is to.resourceType 'sqs'}}
  resource "aws_sns_topic_subscription" "{{@root.name}}_to_sqs_{{to.name}}" {
    topic_arn     = aws_sns_topic.{{@root.name}}.arn
    protocol      = "sqs"
    endpoint      = aws_sqs_queue.{{to.name}}.arn
    {{#if filterPolicy}}
      filter_policy = jsonencode(
        {{{filterPolicy}}}
      )
    {{/if}}
  }
{{/is}}
{{/each}}