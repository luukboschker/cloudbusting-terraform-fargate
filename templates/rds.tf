resource "aws_rds_cluster" "{{name}}" {
  cluster_identifier = "{{name}}-cluster"
  engine = "aurora-postgresql"
  database_name = "{{databasename}}"
  master_username = "{{databasename}}"
  master_password = "Passw0rd!"
  skip_final_snapshot = true
  vpc_security_group_ids = [
    aws_security_group.{{name}}.id
  ]
  tags = {
    {{> default-tags.tf }}
  }
}

resource "aws_rds_cluster_instance" "{{name}}" {
  identifier = "{{name}}-instance"
  engine = "aurora-postgresql"
  cluster_identifier = aws_rds_cluster.{{name}}.id
  instance_class = "{{instancetype}}"
  tags = {
    {{> default-tags.tf }}
  }
}

resource "aws_security_group" "{{name}}" {
  name = "aurora-{{name}}"
  description = "Access to {{name}} Aurora database"
  vpc_id = data.aws_vpc.default.id
  tags = {
    {{> default-tags.tf }}
  }

  ingress {
    protocol = "tcp"
    from_port = 3306
    to_port = 3306
    # Incoming only from ECS services linked to this database
    security_groups = [
      {{#each incoming}}
      {{#is from.resourceType 'ecs-service'}}
        aws_security_group.{{from.name}}.id
      {{/is}}
      {{/each}}
    ]
  }

  egress {
    protocol = -1
    from_port = 0
    to_port = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
}
