provider "aws" {
  profile = "cloudbusting"
  region  = var.aws_region
  version = "2.48"
}
