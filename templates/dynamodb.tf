resource "aws_dynamodb_table" "{{name}}" {
  name = "{{name}}"
  billing_mode = "PAY_PER_REQUEST"
  hash_key = "{{partitionKey}}"
  range_key = "{{sortKey}}"

{{#each (dynamoDbAttributes this) }}
  attribute {
    name = "{{name}}"
    type = "{{type}}"
  }
{{/each}}

{{#if ttl}}
  ttl {
    attribute_name = "{{ttl}}"
    enabled = true
  }
{{/if}}

{{#each gsi }}
  global_secondary_index {
    name = "{{indexName}}"
    hash_key = "{{partitionKey}}"
    projection_type = "ALL"

    {{#if this.sortKey}}
      range_key = "{{sortKey}}"
    {{/if}}
  }
{{/each}}

{{#each lsi }}
  local_secondary_index {
    name = "{{indexName}}"
    hash_key = "{{partitionKey}}"
    projection_type = "ALL"

    {{#if this.sortKey}}
      range_key = "{{sortKey}}"
    {{/if}}
  }
{{/each}}

  tags = {
    {{> default-tags.tf }}
  }
}

{{! Todo: create access policies, assign to incoming connections (ECS) }}