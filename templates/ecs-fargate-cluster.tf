# ECS cluster 
resource "aws_ecs_cluster" "{{name}}" {
  name = "{{name}}"
  tags = {
    {{> default-tags.tf }}
  }
}